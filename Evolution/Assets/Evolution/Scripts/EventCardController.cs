﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EventCardController : MonoBehaviour {
	#region PublicFields
	public EvolutionSkill cardData{
		get{
			return _cardData;
		}
	}
	#endregion
	#region PrivateFields
	[SerializeField]
	private EvolutionSkill _cardData;

	[Space]
	[Header("UI Elements")]
	public Text nameText;
	public Text adaptability;
	public Text ferocity;
	public Text durability;
	
	#endregion

	#region UnityMethods
	private void Awake(){
	}
	private void Start(){
	}
	private void Update(){
	}
	#endregion

	#region PublicMethods
	#endregion
	#region PrivateMethods
	#endregion
}
