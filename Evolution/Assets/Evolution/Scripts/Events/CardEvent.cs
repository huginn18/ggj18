﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.Events;

using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CardEvent :UnityEvent{
}
