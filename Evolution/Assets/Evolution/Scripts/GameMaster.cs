﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {
	#region PublicFields
	/*public int missionsLeft {
		get {
			return _missionsLeft;
		}
		set {
			_missionsLeft = value;
			if (missionsLeft == 0)
				ShowGamesDefeatCanvas ();
		}
	}*/
	public int population {
		get {
			return _population;
		}
		set {
			_population = value;
			if (population >= populationGoal) {
				Debug.Log ("Population goal reached");
				ShowGamesVictoryCanvas ();
			} else if (population <= 0) {
				ShowGamesDefeatCanvas ();
			}
		}
	}
	public int populationGoal;
	public int startPopulation = 500;

	[Space]
	public List<EvolutionSkill> currentMissionEvents;
	public EvolutionSkill currentEvent;
	public List<EvolutionSkill> playerDeck;
	public List<EvolutionSkill> eventDeck;
	public List<EvolutionSkill> hand;

	public Text populationText;

	public List<EvolutionCardController> handInactiveCards;

	public SkillLibrary playerSkillsLib;
	public SkillLibrary compSkillsLib;

	public List<EvolutionCardController> draftHandCards;
	public int cardsLeftToDraft;
	public int handsLeftToDraft;
	
	public MissionsLibrary missionsLibrary;

	public Canvas missionVictory;
	public Canvas missionLoss;

	public Canvas gameLossCanvas;
	public Canvas gameWinCanvas;

	public Canvas background;
	#endregion
	#region PrivateFields
	public int _missionsLeft;
	private int _population;

	[SerializeField]
	private Canvas missionChoiceCanvas;
	[SerializeField]
	private Canvas draftCanvas;
	[SerializeField]
	private Canvas eventCanvas;
	[SerializeField]
	private RectTransform draftHand;
	[SerializeField]
	private RectTransform playHand;
	[SerializeField]
	private EventCardController eventCard;
	private MissionCard currentMission;
	[SerializeField]
	private MissionCardController missionCard;
	#endregion

	#region UnityMethods
	private void Awake () { }
	private void Start () {
		GameStart();
	 }
	private void Update () {
		if((gameWinCanvas.gameObject.activeSelf || gameLossCanvas.gameObject.activeSelf) && Input.GetKeyDown(KeyCode.Space))
			{
				
				foreach (Canvas canvas in FindObjectsOfType<Canvas>())
				{
					canvas.gameObject.SetActive(false);
				}
				population = startPopulation;
				StartMissionChoice();
			}

		if (missionVictory.gameObject.activeSelf && Input.GetKeyDown (KeyCode.Space)) {
			missionVictory.gameObject.SetActive (false);
			StartMissionChoice ();
			return;
		}
		if (missionLoss.gameObject.activeSelf && Input.GetKeyDown (KeyCode.Space)) {
			missionLoss.gameObject.SetActive (false);
			StartMissionChoice ();
		}

	populationText.text = population.ToString();

	}
	#endregion

	#region PublicMethods

	public void GameStart(){
		population = startPopulation;
		StartMissionChoice();
	}

	public void DrawMissions () {
		List<MissionCard> missionHand = missionsLibrary.GetNewMissionHand ();

		GameObject missionsCotainer = GameObject.Find ("Missions");
		for (int i = 0; i < missionsCotainer.transform.childCount; i++) {
			MissionCardController mcc = missionsCotainer.transform.GetChild (i).GetComponent<MissionCardController> ();
			mcc.SetUp(missionHand[i]);
		}
	}
	public void DrawNextMissionEvent () {
		EvolutionSkill newEvent = eventDeck[Random.Range(0, eventDeck.Count)];

		eventDeck.Remove(newEvent);
		
		eventCard.nameText.text = newEvent.SkillName;
		eventCard.adaptability.text = newEvent.Adaptability.ToString();
		eventCard.ferocity.text= newEvent.Ferocity.ToString();
		eventCard.durability.text = newEvent.Durability.ToString();

		currentEvent = Instantiate(newEvent);
	}

	public void OnMissionSelect(MissionCard missionCard)
	{
		currentMission = missionCard;
		StartDeckBuild();
	}

	public void OnCardUse (EvolutionCardController cc) {
		EvolutionSkill card = cc.skill;
		if (currentEvent.Ferocity > 0 && card.Ferocity > 0)
		{
			currentEvent.Ferocity -= card.Ferocity;
			if (currentEvent.Ferocity >= 0)
			eventCard.ferocity.text = currentEvent.Ferocity.ToString();
			else
			eventCard.ferocity.text = 0.ToString();
		}

		if (currentEvent.Durability > 0 && card.Durability > 0)
		{
			currentEvent.Durability -= card.Durability;
			if (currentEvent.Durability >= 0)
			eventCard.durability.text = currentEvent.Durability.ToString();
			else
			eventCard.durability.text = 0.ToString();
		}

		if (currentEvent.Adaptability > 0 && card.Adaptability > 0)
		{
			currentEvent.Adaptability -= card.Adaptability;
			if (currentEvent.Adaptability >= 0)
			eventCard.adaptability.text = currentEvent.Adaptability.ToString();
			else
			eventCard.adaptability.text = 0.ToString();
		}

		// Check if evil is done by any chance
		if (currentEvent.Adaptability <= 0 &&
			currentEvent.Durability <= 0 &&
			currentEvent.Ferocity <= 0) {
			if (eventDeck.Count == 0) {
				Debug.Log ("Mission Done! End Mission!");
				ShowMissionVictoryCanvas ();
				return;
			} else {
				Debug.Log ("Event Done! Draw new Event");
				DrawNextMissionEvent ();
			}			
		}
		cc.gameObject.SetActive(false);
		handInactiveCards.Add(cc);
	}

	public void OnCardDraft(EvolutionCardController dualCard)
	{
		EvolutionSkill skill = dualCard.CardData.PlayerSkill;
		playerDeck.Add(skill);
		dualCard.gameObject.SetActive(false);
		draftHandCards.Remove(dualCard);
		cardsLeftToDraft--;
		if (cardsLeftToDraft == 0)
		{
			foreach(EvolutionCardController card in draftHandCards)
			{
				AddEventToDeck(card);
			}
			handsLeftToDraft--;
			if (handsLeftToDraft == 0)
			{
				StartEvent();
			}
			else
			{
				DrawBuildHand();
			}			
		}
		
	}

	public void AddEventToDeck(EvolutionCardController dualCard)
	{
		eventDeck.Add(dualCard.CardData.ComputerSkill);
		dualCard.gameObject.SetActive(false);
	}
	public void DrawBuildHand () {
		draftHandCards = new List<EvolutionCardController>();
		EvolutionCardController[] draftCards = draftHand.GetComponentsInChildren<EvolutionCardController>(true);
		for (int i = 0; i<5; i++)
		{
			draftCards[i].gameObject.SetActive(true);

			EvolutionCard doubleCard = ScriptableObject.CreateInstance<EvolutionCard>();
			doubleCard.ComputerSkill = currentMission.CardsLibrary.GetNewSkill();
			doubleCard.PlayerSkill = playerSkillsLib.GetNewSkill();

			draftCards[i].CardData = doubleCard;
			draftCards[i]._cardNameText.text = doubleCard.PlayerSkill.SkillName;
			draftCards[i]._playerAdaptability.text = doubleCard.PlayerSkill.Adaptability.ToString ();
			draftCards[i]._playerFerocity.text = doubleCard.PlayerSkill.Ferocity.ToString ();
			draftCards[i]._playerDurability.text = doubleCard.PlayerSkill.Durability.ToString ();
			draftCards[i]._cardNameText2.text = doubleCard.ComputerSkill.SkillName;
			draftCards[i]._computerAdaptability.text = doubleCard.ComputerSkill.Adaptability.ToString();
			draftCards[i]._computerFerocity.text = doubleCard.ComputerSkill.Ferocity.ToString();
			draftCards[i]._computerDurability.text = doubleCard.ComputerSkill.Durability.ToString();

			draftHandCards.Add(draftCards[i]);
		}

		cardsLeftToDraft = 3;
	}

	public void DrawFirstPlayHand() {
		handInactiveCards = new List<EvolutionCardController>();
		EvolutionCardController[] playCards = playHand.GetComponentsInChildren<EvolutionCardController>();
		
		for (int i = 0; i<3; i++)
		{
			playCards[i].gameObject.SetActive(true);

			EvolutionSkill card = playerDeck[Random.Range(0,playerDeck.Count)];

			playerDeck.Remove(card);
			playCards[i]._cardNameText.text = card.SkillName;
			playCards[i]._playerAdaptability.text = card.Adaptability.ToString();
			playCards[i]._playerFerocity.text = card.Ferocity.ToString();
			playCards[i]._playerDurability.text = card.Durability.ToString();
			playCards[i].skill = card;
		}
	}

	public void DrawPlayHand () {
		if (playerDeck.Count == 0)
			ShowMissionDefeatCanvas ();
		else if (playerDeck.Count == 1)
		{
			EvolutionSkill card = playerDeck[0];
			handInactiveCards[0]._cardNameText.text = card.SkillName;
			handInactiveCards[0]._playerAdaptability.text = card.Adaptability.ToString();
			handInactiveCards[0]._playerFerocity.text = card.Ferocity.ToString();
			handInactiveCards[0]._playerDurability.text = card.Durability.ToString();
			handInactiveCards[0].skill = card;
			handInactiveCards[0].gameObject.SetActive(true);

		}
		else
		{
			for (int i = 0; i<2; i++)
			{			

				EvolutionSkill card = playerDeck[Random.Range(0,playerDeck.Count)];

				playerDeck.Remove(card);
				handInactiveCards[i]._cardNameText.text = card.SkillName;
				handInactiveCards[i]._playerAdaptability.text = card.Adaptability.ToString();
				handInactiveCards[i]._playerFerocity.text = card.Ferocity.ToString();
				handInactiveCards[i]._playerDurability.text = card.Durability.ToString();
				handInactiveCards[i].skill = card;
				handInactiveCards[i].gameObject.SetActive(true);
			}
		}

		handInactiveCards.Clear();
	}

	public void DiscardCard (EvolutionCardController card) {
		card.gameObject.SetActive (false);
		handInactiveCards.Add(card);
		DrawPlayHand();
	}
	#endregion
	#region PrivateMethods
	private void ShowGamesVictoryCanvas () {
		gameWinCanvas.gameObject.SetActive (true);
	}
	private void ShowGamesDefeatCanvas () {
		missionLoss.gameObject.SetActive(false);
		gameLossCanvas.gameObject.SetActive (true);
	}
	private void ShowMissionVictoryCanvas () {
		missionVictory.gameObject.SetActive (true);

		population += currentMission.populationGain;
		_missionsLeft--;
	}
	private void ShowMissionDefeatCanvas () {
		missionLoss.gameObject.SetActive (true);

		population -= currentMission.populaitonLoss;
		_missionsLeft--;
	}

	private int CalculateReward () {
		int reward = 0;

		//ToDo: Magic goes here ;( 
		//RadnomFuckery: No one likes magic numbers but everybooooooody loves magic functions <sigh>

		return reward;
	}
	private int CalculatePenalty () {
		int penalty = 0;

		//ToDo: Magic goes here too. ; ; 
		//RandomFuckery: You`are wizard Harry!

		return penalty;
	}

	private void StartMissionChoice()
	{
		background.gameObject.SetActive(true);
		eventCanvas.gameObject.SetActive(false);
		missionChoiceCanvas.gameObject.SetActive(true);
		DrawMissions();
	}

	private void StartDeckBuild () {
		missionChoiceCanvas.gameObject.SetActive (false);
		draftCanvas.gameObject.SetActive (true);

		handsLeftToDraft = 5 + population%500;

		missionCard.SetUp(currentMission);

		DrawBuildHand ();
	}

	private void StartEvent()
	{
		draftCanvas.gameObject.SetActive(false);
		eventCanvas.gameObject.SetActive(true);

		DrawNextMissionEvent();
		DrawFirstPlayHand();
	}
	#endregion
}