﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MissionCardController : MonoBehaviour, IPointerClickHandler{
	#region PublicFields
	#endregion
	#region PrivateFields
	private MissionCard _cardData;

	[Space]
	[Header ("UI Elements")]
	[SerializeField]
	private Text _cardNameText;
	[SerializeField]
	private Text _populationGainText;
	[SerializeField]
	private Text _populationLossText;
	[SerializeField]
	private Text _eventAmountText;
	[SerializeField]
	private Image _cardImage;
	#endregion

	#region UnityMethods
	private void Awake () { }
	private void Start () { }
	private void Update () { }
	#endregion

	#region PublicMethods
	public void SetUp (MissionCard card) {
		_cardNameText.text = card.MissionName;
		_populationGainText.text = card.populationGain.ToString();
		_populationLossText.text = card.populaitonLoss.ToString();
		_eventAmountText.text = card.MissionLength.ToString();
		_cardImage.sprite = card.bcg;
		_cardData = card;
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        FindObjectOfType<GameMaster>().OnMissionSelect(_cardData);
    }
    #endregion
    #region PrivateMethods
    #endregion
}