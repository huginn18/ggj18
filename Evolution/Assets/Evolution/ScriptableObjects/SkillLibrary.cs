﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Evolution/SkillsLib")]
public class SkillLibrary : ScriptableObject {
	#region PublicFields
	public List<EvolutionSkill> skills;
	#endregion
	#region PrivateFields
	#endregion

	#region PublicMethods
	public EvolutionSkill GetNewSkill(){

		Debug.Log(skills.Count);

		int index = Random.Range(0, skills.Count);

		EvolutionSkill newSkill = skills[index];
		//FixMe: Should we really remove those from lib ???
//		skills.Remove(newSkill);

		return newSkill;
	}
	#endregion
	#region PrivateMethods
	#endregion
}
