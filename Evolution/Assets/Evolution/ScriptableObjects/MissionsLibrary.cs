﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName="Evolution/Mission Library")]
public class MissionsLibrary : ScriptableObject {
	#region PublicFields
	#endregion
	#region PrivateFields
	public List<MissionCard> missionCards;
	#endregion

	#region PublicMethods
	public List<MissionCard> GetNewMissionHand(){
		bool allowDuplicates = (missionCards.Count <= 3);
		List<int> usedIndexed = new List<int>();
		while(usedIndexed.Count != 3){
			int i = Random.Range(0, missionCards.Count);
			if(allowDuplicates == false && usedIndexed.Contains(i) == false){
				usedIndexed.Add(i);
			} else if(allowDuplicates){
				usedIndexed.Add(i);
			}
		}

		List<MissionCard> newMissionCardsHand = new List<MissionCard>();
		foreach(int i in usedIndexed){
			newMissionCardsHand.Add(missionCards[i]);
		}
		return newMissionCardsHand;
	}
	#endregion
	#region PrivateMethods
	#endregion
}