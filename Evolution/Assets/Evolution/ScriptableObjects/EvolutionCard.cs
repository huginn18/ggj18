﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.Events;

using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Evolution/EvolutionCard")]
public class EvolutionCard : ScriptableObject {
	#region PublicFields
	public string CardName {
		get{
			return _cardName;
		}
	}

	public EvolutionSkill PlayerSkill{
		get{
			return _playerSkill;
		}
		set{
			_playerSkill = value;
		}
	}
	public EvolutionSkill ComputerSkill{
		get{
			return _computerSkill;
		}
		set{
			_computerSkill = value;
		}
	}
	#endregion
	#region PrivateFields
	[SerializeField]
	private string _cardName;
	[SerializeField]
	private EvolutionSkill _playerSkill;
	[SerializeField]
	private EvolutionSkill _computerSkill;
	#endregion

	#region PublicMethods
	public void Init(EvolutionCardController cardController){
		cardController.OnPlayerDropCallback = () =>{_playerSkill.SkillEvent.Invoke();};
		cardController.OnComputerDropCallback = () => {_computerSkill.SkillEvent.Invoke();};
	}
	#endregion
	#region PrivateMethods
	#endregion
}
