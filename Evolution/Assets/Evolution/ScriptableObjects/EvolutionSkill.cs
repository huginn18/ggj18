﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Evolution/Skill")]
public class EvolutionSkill :ScriptableObject{
	#region PublicFields
	public string SkillName{
		get{
			return _skillName;
		}
	}
	public string SkillDescription{
		get{
			return _skillDescription;
		}
	}
	public UnityEvent SkillEvent{
		get{
			return _skillEvent;
		}
	}

	public int Adaptability{
		get{
			return _versatility;
		}
		set{
			_versatility = value;
		}
	}
	public int Ferocity{
		get{
			return _ferocity;
		}
		set{
			_ferocity = value;
		}
	}
	public int Durability{
		get{
			return _durability;
		}
		set{
			_durability = value;
		}
	}
	#endregion
	#region PrivateFields
	[SerializeField]
	private int _versatility;
	[SerializeField]
	private int _ferocity;
	[SerializeField]
	private int _durability;

	[Space]
	[Header("UI elements")] 
	[SerializeField]
	private string _skillName;
	[SerializeField]
	private string _skillDescription;
	[SerializeField]
	private UnityEvent _skillEvent;
	#endregion

	#region PublicMethods
	#endregion
	#region PrivateMethods
	#endregion
}
