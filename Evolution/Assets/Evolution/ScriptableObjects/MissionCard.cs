﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName="Evolution/Mission Card")]
public class MissionCard : ScriptableObject {
	#region PublicFields
	public string MissionName {
		get{
			return _missionName;
		}
	}
	public int MissionLength {
		get{
			return _missionLength;
		}
	}
	public SkillLibrary CardsLibrary{
		get{
			return _cardsLib;
		}
	}
	public Sprite bcg;
	#endregion
	#region PrivateFields
	[SerializeField]
	private string _missionName;
	[SerializeField]
	private int _missionLength;
	[SerializeField]
	private SkillLibrary _cardsLib;
	public int populationGain;
	public int populaitonLoss;
	#endregion

	#region PublicMethods

	#endregion
	#region PrivateMethods
	#endregion
}
